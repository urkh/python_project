# Python / Pandas test project #

### Installation: ###

#### 1. Clone the repository ####

`git clone git@bitbucket.org:urkh/python_project.git`


#### 2. Make a virtual environment: ####

`virtualenv pandas_test`


#### 3. Activate the env: ####

`source pandas_test/bin/activate`


#### 4. Install required deps: ####

`pip install -r requirements.txt`


#### 5. Move to project folder ####

#### 6. Run the script and see the output: ####

`python app.py`



