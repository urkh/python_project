from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Integer, Float, Date

Base = declarative_base()

class Customers(Base):
    __tablename__ = 'pt_customers'

    id = Column(Integer, primary_key=True)
    order_id = Column(Integer, nullable=False)
    user_id = Column(Integer, nullable=False)
    order_date = Column(Date, nullable=False)
    total_charges_usd = Column(Float, nullable=False)


db_engine = create_engine('sqlite:///python_test.db', echo=False)
