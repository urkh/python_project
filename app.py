from collections import OrderedDict
from datetime import datetime
from sqlalchemy.orm import sessionmaker
from dateutil.relativedelta import relativedelta
from db import db_engine, Customers
import pandas as pd
import os


def import_csv(file):
    for customers in pd.read_csv(os.getcwd()+'/'+file, sep=';',  parse_dates=[0], iterator=True):
        customers.index += 1
        customers['Order_Date'] = pd.to_datetime(customers.Order_Date, format='%d.%m.%Y')
        customers.to_sql('pt_customers', db_engine, if_exists='replace', index=True, index_label='id')

    query_data()


def import_xlsx(file):
    xls = pd.ExcelFile(os.getcwd()+'/'+file)
    customers = xls.parse(xls.sheet_names[0])
    customers.index += 1
    customers['Order_Date'] = pd.to_datetime(customers.Order_Date, format='%d.%m.%Y')
    customers.to_sql('pt_customers', db_engine, if_exists='replace', index=True, index_label='id')

    query_data()


def query_data():
    Session = sessionmaker(bind=db_engine)
    session = Session()
    customers = session.query(Customers.order_date, Customers.user_id).order_by(Customers.order_date)
    dataframe_db = pd.read_sql(customers.statement, db_engine, chunksize=1)
    users_bought = [[]]
    users_sorted = {}
    first_date = None
    user_counter = 0
    for row in dataframe_db:

        if first_date is None:
            first_date = row.order_date[0].strftime('%Y-%m')  # Set initial purchase dates

        if first_date == row.order_date[0].strftime('%Y-%m'):
            if len(users_bought[0]) == 0:
                user_counter += 1
                users_bought[0].append({'date': first_date, 'users': user_counter})
                users_sorted[first_date] = [row.user_id[0]]
            else:
                if row.user_id[0] not in users_sorted[first_date]:
                    user_exists = [k for k, v in users_sorted.iteritems() if row.user_id[0] in v]
                    if len(user_exists) == 0:
                        index = users_bought[0].index({'date': first_date, 'users': user_counter})
                        user_counter += 1
                        users_bought[0][index]['users'] = user_counter
                        users_sorted[first_date].append(row.user_id[0])
                    else:
                        users_sorted[first_date].append(row.user_id[0])
        else:
            first_date = row.order_date[0].strftime('%Y-%m')
            users_sorted[first_date] = [row.user_id[0]]
            user_counter = 0
            users_bought[0].append({'date': first_date, 'users': user_counter})

        first_date = row.order_date[0].strftime('%Y-%m')

    users_sorted = OrderedDict(sorted(users_sorted.items(), key=lambda t: t[0]))

    dates = [x['date'] for x in users_bought[0]]
    for current_date in dates:
        if len(users_bought) == 1:
            for row_date, row_users in users_sorted.iteritems():
                if datetime.strptime(row_date, '%Y-%m') > datetime.strptime(current_date, '%Y-%m'):
                    new_date = {}
                    user_counter = 0
                    for user in row_users:
                        if user in users_sorted[current_date] and user in users_sorted[row_date]:
                            user_counter += 1
                            new_date['date'] = row_date
                            new_date['users'] = user_counter
                    users_bought.append([new_date])
        else:
            temp_index = 1
            for row_date, row_users in users_sorted.iteritems():
                if datetime.strptime(row_date, '%Y-%m') > datetime.strptime(current_date, '%Y-%m'):
                    new_date = {}
                    user_counter = 0

                    for user in row_users:
                        for dd in dates:
                            if user in users_sorted[dd]:
                                date_found = dd
                                break

                        if user in users_sorted[current_date] and user in users_sorted[row_date] and date_found == current_date:
                            user_counter += 1
                            new_date['date'] = row_date
                            new_date['users'] = user_counter

                    if len(new_date) == 0:
                        new_date['date'] = row_date
                        new_date['users'] = 0

                    users_bought[temp_index].append(new_date)
                    temp_index += 1

    # import ipdb; ipdb.set_trace()
    headers = [x['date'] for x in users_bought[0]]
    vals = [[y['users'] for y in x] for x in users_bought]
    df = pd.DataFrame(vals, columns=headers, index=[str(vals.index(x)+1)+'. Month buy' for x in vals]).fillna(0).astype(int)
    df.to_csv('Result.csv', sep=',')

    print "\nSee result in 'Result.csv' file"


if __name__ == "__main__":
    try:
        _from = int(raw_input('Read file: \n(1) from csv \n(2) from xlsx\n'))
        if _from == 1:
            import_csv('OrderDB.csv')
        else:
            import_xlsx('OrderDB.xlsx')

    except ValueError:
        print "Enter a number"
